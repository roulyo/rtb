#pragma once

#include <mutex>

namespace rtb
{

    class AutoLock
    {
    public:
        AutoLock(std::mutex& _mutex)
            : m_Mutex(_mutex)
        {
            m_Mutex.lock();
        }

        ~AutoLock()
        {
            m_Mutex.unlock();
        }

    private:
        std::mutex& m_Mutex;

    };

}

#define rtb_ScopedLock(mtx)   rtb::AutoLock _lock_##mtx(mtx)
