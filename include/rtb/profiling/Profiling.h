#pragma once

#if defined RTB_PROFILING_ENABLED

#include <chrono>
#include <mutex>
#include <map>
#include <list>
#include <thread>

#include <rtb/utils/class_utils.h>

namespace rtb
{
    namespace time
    {
        static const unsigned int second        = 1000000000;
        static const unsigned int millisecond   = 1000000;
        static const unsigned int microsecond   = 1000;
        static const unsigned int nanosecond    = 1;
    }

    //------------------------------------------------------------------------
    class ProfileResult;

    //------------------------------------------------------------------------
    class Profiler
    {
        rtb_NonInstantiable(Profiler);

    public:
        static void Start();
        static void Stop();

        static void ReportResult(const ProfileResult& _result);

        static void SetTagThresholdNs(unsigned long long _threshold);
        static void SetTotalThresholdNs(unsigned long long _threshold);

    private:
        static void WriteReport();

    private:
        static unsigned long long                                   m_TagThresholdNs;
        static unsigned long long                                   m_TotalThresholdNs;

        static std::chrono::high_resolution_clock::time_point       m_StartTime;
        static std::map<std::thread::id, std::list<ProfileResult>>  m_Tags;
        static std::mutex                                           m_Mutex;
        static bool                                                 m_IsProfiling;

    };

    //------------------------------------------------------------------------
    class ProfileResult
    {
    public:
        ProfileResult(unsigned long long _duration, std::thread::id _threadId, std::string _tagName)
            : m_DurationNs(_duration)
            , m_ThreadId(_threadId)
            , m_TagName(_tagName)
        {
        }

    public:
        rtb_CONST_property(unsigned long long,  DurationNs);
        rtb_CONST_property(std::thread::id,     ThreadId);
        rtb_CONST_property(std::string,         TagName);

    };

    //------------------------------------------------------------------------
    class ProfileTag
    {
    public:
        ProfileTag(const std::string& _tagName)
            : m_StartTime()
            , m_DurationNs(0)
            , m_ThreadId(std::this_thread::get_id())
            , m_TagName(_tagName)
        {
            m_StartTime = std::chrono::high_resolution_clock::now();
        }

        ~ProfileTag()
        {
            m_DurationNs = (std::chrono::high_resolution_clock::now() - m_StartTime).count();
            Profiler::ReportResult(ProfileResult(m_DurationNs, m_ThreadId, m_TagName));
        }

    // private:
    //     static std::map<std::thread::id, unsigned int>  m_

    private:
        std::chrono::high_resolution_clock::time_point  m_StartTime;
        unsigned long long                              m_DurationNs;

        const std::thread::id                           m_ThreadId;
        const std::string                               m_TagName;

    };

}

#   define rtb_StartProfiling()    rtb::Profiler::Start()
#   define rtb_Profile(tag)        rtb::ProfileTag _##tag(#tag)
#   define rtb_StopProfiling()     rtb::Profiler::Stop()

#else

#   define rtb_StartProfiling()    ((void) 0)
#   define rtb_Profile(tag)        ((void) 0)
#   define rtb_StopProfiling()     ((void) 0)

#endif

#include "Profiling.inl"
