#pragma once

#if defined(RTB_LOGGING_ENABLED)

#include <list>
#include <mutex>

#include <rtb/utils/class_utils.h>
#include <rtb/utils/StringId.h>

namespace rtb
{

    static const size_t LOG_BUFFER_SIZE = 2048;

    class Logger;

    enum class LogLevel
    {
        Debug,
        Info,
        Warning,
        Error,
    };

    typedef StringIdStr LogCategory;

    class IQuill
    {
    public:
        IQuill();
        virtual ~IQuill() = 0;

        virtual void OnLog(const LogCategory& _category, LogLevel _level, const char* const _log) = 0;

        //void setEnabledLogs(const std::list<LogCategory>& _categories);
        void SetLogLevel(LogLevel _level);

        bool CanLog(const LogCategory& _category, LogLevel _level);

        //void registerLogCategory(const LogCategory& _category);


    protected:
        //std::map<LogCategory, bool>  m_logCategories;
        LogLevel                     m_LogLevel;

    };


    class Logger
    {
        rtb_NonInstantiable(Logger);

    public:
        static void Log(const LogCategory& _category, LogLevel _level, const char* _log, ...);
        static void RegisterQuill(IQuill& _quill);

    private:
        static void CleanLog();
        static void LogV(const LogCategory& _category, LogLevel _level, const char* _log, va_list _args);
        static void DispatchLog(const LogCategory& _category, LogLevel _level);

    private:
        static std::list<IQuill*>   m_Quills;
        static char                 m_Buffer[LOG_BUFFER_SIZE];
        static size_t               m_WriteLen;

        static std::mutex           m_Mutex;
    };

}

#   define rtb_LogDebug(Category, str, ...)     rtb::Logger::Log(Category, rtb::LogLevel::Debug, str,  ##__VA_ARGS__);
#   define rtb_LogInfo(Category, str, ...)      rtb::Logger::Log(Category, rtb::LogLevel::Info, str,  ##__VA_ARGS__);
#   define rtb_LogWarning(Category, str, ...)   rtb::Logger::Log(Category, rtb::LogLevel::Warning, str,  ##__VA_ARGS__);
#   define rtb_LogError(Category, str, ...)     rtb::Logger::Log(Category, rtb::LogLevel::Error, str,  ##__VA_ARGS__);

#else

#   define rtb_LogDebug(Category, Stream)       ((void) 0)
#   define rtb_LogInfo(Category, Stream)        ((void) 0)
#   define rtb_LogWarning(Category, Stream)     ((void) 0)
#   define rtb_LogError(Category, Stream)       ((void) 0)

#endif

#include "Logging.inl"