#pragma once

#include <cstdint>
#include <cstddef>

namespace rtb
{
    // Generate CRC lookup table
    template <unsigned c, int k = 8>
    struct f : f<((c & 1) ? 0xedb88320 : 0) ^ (c >> 1), k - 1> {};
    template <unsigned c> struct f<c, 0> { enum : unsigned { value = c }; };

#define __A(x) __B(x) __B(x + 128)
#define __B(x) __C(x) __C(x +  64)
#define __C(x) __D(x) __D(x +  32)
#define __D(x) __E(x) __E(x +  16)
#define __E(x) __F(x) __F(x +   8)
#define __F(x) __G(x) __G(x +   4)
#define __G(x) __H(x) __H(x +   2)
#define __H(x) __I(x) __I(x +   1)
#define __I(x) f<x>::value,

    static constexpr unsigned crc_table[] = { __A(0) };

    // Constexpr implementation and helpers
    inline constexpr uint32_t crc32_impl(const char* const p, size_t len, uint32_t crc) {
        return len ?
            crc32_impl(p + 1, len - 1, (crc >> 8) ^ crc_table[(crc & 0xFF) ^ *p])
            : crc;
    }

    inline constexpr uint32_t crc32(const char* const data, size_t length) {
        return ~crc32_impl(data, length, ~(unsigned)0);
    }

    inline constexpr size_t strlen_c(const char* const  str) {
        return *str ? 1 + strlen_c(str + 1) : 0;
    }

    inline constexpr int WSID(const char* const str) {
        return crc32(str, strlen_c(str));
    }

}