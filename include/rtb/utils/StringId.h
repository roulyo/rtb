#pragma once

#include <rtb/utils/crc32.h>
#include <string>

namespace rtb
{

    class StringId
    {
        friend std::hash<rtb::StringId>;

    public:
        constexpr StringId()
            : m_Crc(0)
        {
        }

        constexpr StringId(const char* const _string)
            : m_Crc(WSID(_string))
        {
        }

        constexpr StringId(const StringId& _stringId)
            : m_Crc(_stringId.m_Crc)
        {
        }

        constexpr StringId(int _crc)
            : m_Crc(_crc)
        {
        }

        constexpr const StringId& operator=(const StringId& _other) const
        {
            *(const_cast<int*>(&m_Crc)) = _other.m_Crc;

            return *this;
        }

        constexpr bool operator==(const StringId& _other) const
        {
            return m_Crc == _other.m_Crc;
        }

        constexpr bool operator!=(const StringId& _other) const
        {
            return m_Crc != _other.m_Crc;
        }

        constexpr bool operator<(const StringId& _other) const
        {
            return m_Crc < _other.m_Crc;
        }

        constexpr operator int() const
        {
            return m_Crc;
        }

    protected:
        const int   m_Crc;

    };


    class StringIdStr : public StringId
    {
    public:
        StringIdStr(const char _string[])
            : StringId(_string)
            , m_String(_string)
        {
        }

        const std::string& GetString() const
        {
            return m_String;
        }

    private:
        const std::string m_String;

    };

}

namespace std
{
    template <> struct hash<rtb::StringId>
    {
        typedef rtb::StringId argument_type;
        typedef std::size_t result_type;

        std::size_t operator()(const rtb::StringId& id) const noexcept
        {
            return std::hash<std::size_t>()(id.m_Crc);
        }
    };
}
