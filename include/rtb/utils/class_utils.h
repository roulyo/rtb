#pragma once

#define rtb_RW_property(type, name)                                         \
    public:                                                                 \
        inline const type& Get##name() const { return m_##name; }           \
        inline void Set##name(const type& _##name) { m_##name = _##name; }  \
    protected:                                                              \
        type m_##name;

#define rtb_RO_property(type, name)                                         \
    public:                                                                 \
        inline const type& Get##name() const { return m_##name; }           \
    protected:                                                              \
        type m_##name;

#define rtb_CONST_property(type, name)                                      \
    public:                                                                 \
        inline const type& Get##name() const { return m_##name; }           \
    protected:                                                              \
        const type m_##name;

#define rtb_CREF_property(type, name)                                       \
    public:                                                                 \
        inline const type& Get##name() const { return m_##name; }           \
    protected:                                                              \
        const type& m_##name;

#define rtb_NonInstantiable(Class)                                          \
    Class() = delete;                                                       \
    ~Class() = delete;
