#if defined RTB_LOGGING_ENABLED

#include <assert.h>
#include <cstring>
#include <stdarg.h>
#include <stdio.h>

#include <rtb/utils/AutoLock.h>

namespace rtb
{
    //------------------------------------------------------------------------
    inline IQuill::IQuill()
    {

    }

    //------------------------------------------------------------------------
    inline IQuill::~IQuill()
    {
    }


    //------------------------------------------------------------------------
    inline void OnLog(const LogCategory& _category, LogLevel _level, const char* const _log)
    {

    }

    //------------------------------------------------------------------------
    //void IQuill::setEnabledLogs(const std::list<LogCategory>& _categories)
    //{
    //    for (std::pair<const LogCategory, bool>& logEntry : m_logCategories)
    //    {
    //        logEntry.second = false;
    //    }

    //    for (const LogCategory& category : _categories)
    //    {
    //        std::map<LogCategory, bool>::iterator logEntry = m_logCategories.find(category);

    //        assert(logEntry != m_logCategories.end());
    //        logEntry->second = true;
    //    }
    //}

    //------------------------------------------------------------------------
    inline void IQuill::SetLogLevel(LogLevel _level)
    {
        m_LogLevel = _level;
    }

    //------------------------------------------------------------------------
    inline bool IQuill::CanLog(const LogCategory& _category, LogLevel _level)
    {
        return true; /*m_logCategories.at(_category) && _level <= m_logLevel*/;
    }

    //------------------------------------------------------------------------
    //void IQuill::registerLogCategory(const LogCategory& _category)
    //{
    //    assert(m_logCategories.find(_category) == m_logCategories.end());

    //    m_logCategories[_category] = true;
    //}

    //------------------------------------------------------------------------
    inline size_t              Logger::m_WriteLen = 0;
    inline char                Logger::m_Buffer[LOG_BUFFER_SIZE];
    inline std::list<IQuill*>  Logger::m_Quills;
    inline std::mutex          Logger::m_Mutex;

    //------------------------------------------------------------------------
    inline void Logger::RegisterQuill(IQuill& _quill)
    {
        m_Quills.push_back(&_quill);
    }

    //------------------------------------------------------------------------
    inline void Logger::CleanLog()
    {
        std::memset(m_Buffer, 0, m_WriteLen);
    }

    //------------------------------------------------------------------------
    inline void Logger::Log(const LogCategory& _category, LogLevel _level, const char* const _log, ...)
    {
        va_list args;

        va_start(args, _log);
        LogV(_category, _level, _log, args);
        va_end(args);
    }

    //------------------------------------------------------------------------
    inline void Logger::LogV(const LogCategory& _category, LogLevel _level, const char* const _log, va_list _args)
    {
        rtb_ScopedLock(m_Mutex);

        CleanLog();
        //m_WriteLen = vsprintf_s(m_Buffer, _log, _args);
        m_WriteLen = vsnprintf(m_Buffer, LOG_BUFFER_SIZE, _log, _args);
        DispatchLog(_category, _level);
    }

    //------------------------------------------------------------------------
    inline void Logger::DispatchLog(const LogCategory& _category, LogLevel _level)
    {
        for (IQuill* quill : m_Quills)
        {
            if (quill->CanLog(_category, _level))
            {
                quill->OnLog(_category, _level, m_Buffer);
            }
        }
    }

}

#endif