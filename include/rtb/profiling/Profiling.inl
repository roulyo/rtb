#if defined RTB_PROFILING_ENABLED

#include <assert.h>

#include <ctime>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

#include <rtb/utils/AutoLock.h>

namespace rtb
{
    //------------------------------------------------------------------------
    inline unsigned long long                                   Profiler::m_TagThresholdNs = 0;
    inline unsigned long long                                   Profiler::m_TotalThresholdNs = 0;
    inline std::chrono::high_resolution_clock::time_point       Profiler::m_StartTime;
    inline std::map<std::thread::id, std::list<ProfileResult>>  Profiler::m_Tags;
    inline std::mutex                                           Profiler::m_Mutex;
    inline bool                                                 Profiler::m_IsProfiling = false;

    //------------------------------------------------------------------------
    inline void Profiler::Start()
    {
        rtb_ScopedLock(m_Mutex);

        assert(!m_IsProfiling);
        assert(m_Tags.empty());
        m_IsProfiling = true;
        m_StartTime = std::chrono::high_resolution_clock::now();
    }

    //------------------------------------------------------------------------
    inline void Profiler::Stop()
    {
        rtb_ScopedLock(m_Mutex);

        assert(m_IsProfiling);

        unsigned long long totalTime = (std::chrono::high_resolution_clock::now() - m_StartTime).count();

        if (totalTime >= m_TotalThresholdNs)
        {
            WriteReport();
        }

        m_IsProfiling = false;
        m_Tags.clear();
    }

    //------------------------------------------------------------------------
    inline void Profiler::ReportResult(const ProfileResult& _result)
    {
        rtb_ScopedLock(m_Mutex);

        if (m_IsProfiling && _result.GetDurationNs() > m_TagThresholdNs)
        {
            m_Tags[_result.GetThreadId()].push_back(_result);
        }
    }

    //------------------------------------------------------------------------
    inline void Profiler::WriteReport()
    {
        std::time_t t = std::time(nullptr);
        auto time = std::put_time(std::localtime(&t), "%d-%m-%y@%Hh%Mm%Ss");

        std::stringstream filename;
        filename << "profiling_report[" << time << "].txt";

        std::ofstream reportFile;
        reportFile.open(filename.str().c_str(), std::ofstream::out);

        assert(reportFile.is_open());

        for (const std::pair<std::thread::id, std::list<ProfileResult>>& tags : m_Tags)
        {
            reportFile << "Thread " << tags.first << ":" << std::endl;

            for (const ProfileResult& tag : tags.second)
            {
                reportFile << "    - " << tag.GetTagName().c_str() << ": " << std::setprecision(4);

                if (tag.GetDurationNs() > time::second)
                {
                    reportFile << static_cast<double>(tag.GetDurationNs()) / time::second << "s";
                }
                else if (tag.GetDurationNs() > time::millisecond)
                {
                    reportFile << static_cast<double>(tag.GetDurationNs()) / time::millisecond << "ms";
                }
                else if (tag.GetDurationNs() > time::microsecond)
                {
                    reportFile << static_cast<double>(tag.GetDurationNs()) / time::microsecond << "us";
                }
                else
                {
                    reportFile << tag.GetDurationNs() << "ns";
                }

                reportFile << std::endl;
            }
        }

        reportFile.flush();
        reportFile.close();
    }

    //------------------------------------------------------------------------
    inline void Profiler::SetTagThresholdNs(unsigned long long _threshold)
    {
        m_TagThresholdNs = _threshold;
    }

    //------------------------------------------------------------------------
    inline void Profiler::SetTotalThresholdNs(unsigned long long _threshold)
    {
        m_TotalThresholdNs = _threshold;
    }
}

#endif
