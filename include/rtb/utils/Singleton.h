#pragma once

#include <cassert>

namespace rtb
{

    template<class T>
    class Singleton
    {
    public:
        static void Instantiate();
        static T& GetInstance();
        static void Release();

    protected:
        Singleton()
        {
            //static_assert(!std::is_default_constructible<T>::value, "Singleton has a public default constructor! Use rtb_ImplSingleton(T) to set things right.");

            // Somehow a T object was created outside the Singleton pattern.
            // Class T probably has a non-default public constructor.
            assert(m_Instance == nullptr);
        }

        ~Singleton() {}

        virtual void InitInstance() {}
        virtual void DestroyInstance() {}
    
    protected:
        static T* m_Instance;

    };

    template<class T>
    T* Singleton<T>::m_Instance = nullptr;

    template<class T>
    T& Singleton<T>::GetInstance()
    {
        return *m_Instance;
    }

    template<class T>
    void Singleton<T>::Release()
    {
        m_Instance->DestroyInstance();
        delete m_Instance;
        m_Instance = nullptr;
    }

    template<class T>
    void Singleton<T>::Instantiate()
    {
        m_Instance = new T();
        m_Instance->InitInstance();
    }

}

#define rtb_DeclSingleton(Class)            \
        friend class rtb::Singleton<Class>; \
    private:                                \
        Class() : Singleton() {}            \
        ~Class() {}
        
