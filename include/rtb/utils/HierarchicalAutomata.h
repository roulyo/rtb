#pragma once

#include <algorithm>
#include <functional>
#include <map>
#include <vector>

namespace rtb
{

//----------------------------------------------------------------------------------------------------------------------
    typedef int StateId;

//----------------------------------------------------------------------------------------------------------------------
    class HierarchicalAutomata
    {
    public:
        static constexpr StateId NullStateId = 0xFFFFFFFF;

    public:
        void Tick();

        bool RegisterState(StateId _stateId,
                           StateId _parentStateId,
                           const std::function<void()>& _enterFunc,
                           const std::function<void()>& _updateFunc,
                           const std::function<void()>& _exitFunc);

        void AddUnidirectionalLink(StateId _fromStateId, StateId _toStateId);
        void AddBidirectionalLink(StateId _stateId1, StateId _stateId2);

        bool RequestState(StateId _stateId, bool _force = false);
        void ResetAutomata();

        StateId GetCurrentStateId() const;

    private:
        void PushState(StateId _stateId);
        void PopState();

        bool IsParentOf(StateId _parentId, StateId _childId);
        std::vector<StateId> GetParents(StateId _stateId, StateId _parentIdLimit = NullStateId);

    private:
        struct HAState
        {
            StateId Id;
            StateId ParentId;
            std::function<void()> EnterCallback;
            std::function<void()> UpdateCallback;
            std::function<void()> ExitCallback;

        };

    private:
        std::map<StateId, HAState>              m_States;
        std::map<StateId, std::vector<StateId>> m_Links;
        std::vector<HAState*>                   m_ActiveStates;

        StateId m_RequestedStateId  = NullStateId;
        bool    m_ResetRequested    = false;

    };

//----------------------------------------------------------------------------------------------------------------------
    inline void HierarchicalAutomata::Tick()
    {
        StateId requestedStateId = m_RequestedStateId;

        if (m_ResetRequested)
        {
            m_ResetRequested = false;
            m_RequestedStateId = NullStateId;

            while (!m_ActiveStates.empty())
            {
                PopState();
            }
        }
        else if (requestedStateId != NullStateId)
        {
            m_RequestedStateId = NullStateId;

            while (   !m_ActiveStates.empty()
                   && requestedStateId != GetCurrentStateId()
                   && !IsParentOf(GetCurrentStateId(), requestedStateId))
            {
                PopState();
            }

            StateId parentIdLimit = m_ActiveStates.empty() ? NullStateId : GetCurrentStateId();
            std::vector<StateId> statesToPush = GetParents(requestedStateId, parentIdLimit);

            for (auto state = statesToPush.crbegin(); state != statesToPush.crend(); ++state)
            {
                PushState(*state);
            }

            if (m_ActiveStates.empty() || requestedStateId != GetCurrentStateId())
            {
                PushState(requestedStateId);
            }
        }

        // if (m_RequestedStateId != NullStateId && m_RequestedStateId != requestedStateId)
        //     return;

        for (HAState* state : m_ActiveStates)
        {
            state->UpdateCallback();
        }
    }

//----------------------------------------------------------------------------------------------------------------------
    inline bool HierarchicalAutomata::RegisterState(StateId _stateId,
                                                    StateId _parentStateId,
                                                    const std::function<void()> & _enterFunc,
                                                    const std::function<void()> & _updateFunc,
                                                    const std::function<void()> & _exitFunc)
    {
        if (m_States.find(_stateId) != m_States.cend())
            return false;

        HAState state;
        state.Id = _stateId;
        state.ParentId = _parentStateId;
        state.EnterCallback = _enterFunc;
        state.UpdateCallback = _updateFunc;
        state.ExitCallback = _exitFunc;

        m_States[_stateId] = state;

        if (_parentStateId != NullStateId)
        {
            AddBidirectionalLink(_stateId, _parentStateId);
        }

        return true;
    }

//----------------------------------------------------------------------------------------------------------------------
    inline void HierarchicalAutomata::AddUnidirectionalLink(StateId _fromStateId,
                                                            StateId _toStateId)
    {
        m_Links[_fromStateId].push_back(_toStateId);
    }

//----------------------------------------------------------------------------------------------------------------------
    inline void HierarchicalAutomata::AddBidirectionalLink(StateId _stateId1,
                                                           StateId _stateId2)
    {
        m_Links[_stateId1].push_back(_stateId2);
        m_Links[_stateId2].push_back(_stateId1);
    }

//----------------------------------------------------------------------------------------------------------------------
    inline bool HierarchicalAutomata::RequestState(StateId _stateId,
                                                   bool _force /* = false */)
    {
        if (!_force && m_RequestedStateId != NullStateId)
            return false;

        if (m_ResetRequested)
            return false;

        if (m_ActiveStates.empty())
        {
            m_RequestedStateId = _stateId;

            return true;
        }

        bool linksWithCurrent = std::find(
                                    m_Links[GetCurrentStateId()].cbegin(),
                                    m_Links[GetCurrentStateId()].cend(),
                                    _stateId)
                                != m_Links[GetCurrentStateId()].cend();

        if (   linksWithCurrent
            || IsParentOf(GetCurrentStateId(), _stateId)
            || IsParentOf(_stateId, GetCurrentStateId()))
        {
            m_RequestedStateId = _stateId;

            return true;
        }

        return false;
    }

//----------------------------------------------------------------------------------------------------------------------
    inline void HierarchicalAutomata::ResetAutomata()
    {
       m_ResetRequested = true;
    }

//----------------------------------------------------------------------------------------------------------------------
    inline StateId HierarchicalAutomata::GetCurrentStateId() const
    {
        return m_ActiveStates.back()->Id;
    }

//----------------------------------------------------------------------------------------------------------------------
    inline void HierarchicalAutomata::PushState(StateId _stateId)
    {
        HAState* state = &m_States[_stateId];
        m_ActiveStates.push_back(state);
        state->EnterCallback();
    }

//----------------------------------------------------------------------------------------------------------------------
    inline void HierarchicalAutomata::PopState()
    {
        HAState* state = m_ActiveStates.back();
        m_ActiveStates.pop_back();
        state->ExitCallback();
    }

//----------------------------------------------------------------------------------------------------------------------
    inline bool HierarchicalAutomata::IsParentOf(StateId _parentId, StateId _childId)
    {
        HAState* child = &m_States[_childId];

        while (child->ParentId != NullStateId && child->ParentId != _parentId)
        {
            child = &m_States[child->ParentId];
        }

        return child->ParentId != NullStateId;
    }

//----------------------------------------------------------------------------------------------------------------------
    inline std::vector<StateId> HierarchicalAutomata::GetParents(StateId _stateId,
                                                                 StateId _parentIdLimit)
    {
        std::vector<StateId> parents;
        HAState* current = &m_States[_stateId];

        while (current->ParentId != NullStateId && current->ParentId != _parentIdLimit)
        {
            current = &m_States[current->ParentId];
            parents.push_back(current->Id);
        }

        return parents;
    }

}