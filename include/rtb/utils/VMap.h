#pragma once

#include <cassert>
#include <memory>
#include <utility>
#include <vector>


namespace rtb
{
//----------------------------------------------------------------------------------------------------------------------
    template<class Key,
             class T,
             class Allocator = std::allocator<std::pair<Key, T> > >
    class VMap
    {
    public:
        //using _Nodeptr = typename _Mybase::_Nodeptr;
        using key_type = Key;
        using mapped_type = T;
        //using value_compare = typename _Mybase::value_compare;
        using value_type = std::pair<Key, T>;
        using allocator_type = Allocator;

        using _Mybase = std::vector<value_type, allocator_type>;
        using size_type = typename _Mybase::size_type;
        using difference_type = typename _Mybase::difference_type;
        using pointer = typename _Mybase::pointer;
        using const_pointer = typename _Mybase::const_pointer;
        using reference = value_type&;
        using const_reference = const value_type&;
        using iterator = typename _Mybase::iterator;
        using const_iterator = typename _Mybase::const_iterator;
        using reverse_iterator = typename _Mybase::reverse_iterator;
        using const_reverse_iterator = typename _Mybase::const_reverse_iterator;

    public:
        T& at(const Key& _key)
        {
            for (auto it = m_Vector.begin(); it != m_Vector.end(); ++it)
                if (_key == it->first) return it->second;

            assert(false);
            return m_Vector.end()->second;
        }

        const T& at(const Key& _key) const
        {
            for (auto it = m_Vector.cbegin(); it != m_Vector.cend(); ++it)
                if (_key == it->first) return it->second;

            assert(false);
            return m_Vector.cend()->second;
        }

        T& operator[](const Key& _key)
        {
            for (auto it = m_Vector.begin(); it != m_Vector.end(); ++it)
                if (_key == it->first) return it->second;

            m_Vector.push_back({ _key, T() });
            return (m_Vector.end() - 1)->second;
        }

        iterator begin() { return m_Vector.begin(); }
        const_iterator begin() const { return m_Vector.begin(); }
        const_iterator cbegin() const { return m_Vector.cbegin(); }
        iterator end() { return m_Vector.end(); }
        const_iterator end() const { return m_Vector.end(); }
        const_iterator cend() const { return m_Vector.cend(); }
        reverse_iterator rbegin() { return m_Vector.rbegin(); }
        const_reverse_iterator crbegin() const { return m_Vector.crbegin(); }
        reverse_iterator rend() { return m_Vector.rend(); }
        const_reverse_iterator crend() const { return m_Vector.crend(); }

        bool empty() const { return m_Vector.empty(); }
        size_type size() const { return m_Vector.size(); }

        void clear() { return m_Vector.clear(); }
        iterator erase(iterator _pos) { return m_Vector.erase(_pos); }
        size_type erase(const Key& _key)
        {
            for (auto it = m_Vector.begin(); it != m_Vector.end(); ++it)
            {
                if (_key == it->first)
                {
                    erase(it);
                    return 1;
                }
            }

            return 0;
        }

        size_type count(const Key& _key) const
        {
            for (auto it = m_Vector.cbegin(); it != m_Vector.cend(); ++it)
                if (_key == it->first) return 1;

            return 0;
        }

        iterator find(const Key& _key)
        {
            auto it = m_Vector.begin();
            for (; it != m_Vector.end(); ++it)
                if (_key == it->first) break;

            return it;
        }

        const_iterator find(const Key& _key) const
        {
            auto it = m_Vector.cbegin();
            for (; it != m_Vector.cend(); ++it)
                if (_key == it->first) break;

            return it;
        }

    private:
        std::vector<value_type, Allocator> m_Vector;

    };

}
